# Spark

Docker Image used in Iotplatform project.

This image is based in Spark 2.4.0 Spark-Kubernetes Image. More info about Spark-Kubernetes implementation in <https://spark.apache.org/docs/2.3.1/running-on-kubernetes.html>.

Image that contains Java, Spark and spark-batch-processing Jar (Iotplatform). It executes spark-batch-processing in a Kubernetes cluster.

## Usage

Execute `spark submit` targeting a Kubernetes API server:

```bash
spark-submit \
    --master k8s://https://<k8s-apiserver-host>:<k8s-apiserver-port> \
    --deploy-mode cluster \
    --name <app-name> \
    --class main.Main \
    --conf spark.executor.instances=<num-parallel-executors> \
    --conf spark.kubernetes.container.image=registry.gitlab.com/tum-iot-lab/spark:latest \
    local:///local:///opt/spark/jars-examples/spark-batch-processing-0.0.1-SNAPSHOT.jar
```