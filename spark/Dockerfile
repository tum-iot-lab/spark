FROM openjdk:8-alpine

ARG spark_jars=jars
ARG img_path=kubernetes/dockerfiles
ARG k8s_tests=kubernetes/tests

RUN set -ex && \
    apk upgrade --no-cache && \
    apk add --no-cache bash tini libc6-compat linux-pam && \
    mkdir -p /opt/spark && \
    mkdir -p /opt/spark/work-dir && \
    touch /opt/spark/RELEASE && \
    rm /bin/sh && \
    ln -sv /bin/bash /bin/sh && \
    echo "auth required pam_wheel.so use_uid" >> /etc/pam.d/su && \
    chgrp root /etc/passwd && chmod ug+rw /etc/passwd
	

# SPARK
ENV SPARK_VERSION 2.4.0
ENV HADOOP_VERSION 2.7
ENV SPARK_PACKAGE spark-${SPARK_VERSION}-bin-hadoop${HADOOP_VERSION}
ENV SPARK_HOME /opt/spark
ENV PATH $PATH:${SPARK_HOME}/bin

RUN wget "http://mirror.synyx.de/apache/spark/spark-${SPARK_VERSION}/${SPARK_PACKAGE}.tgz" &&\ 
	tar -xzf /$SPARK_PACKAGE.tgz -C /usr/

RUN cp -r /usr/$SPARK_PACKAGE/${spark_jars} /opt/spark/jars &&\
	cp -r /usr/$SPARK_PACKAGE/bin /opt/spark/bin &&\
	cp -r /usr/$SPARK_PACKAGE/sbin /opt/spark/sbin &&\
	cp -r /usr/$SPARK_PACKAGE/${img_path}/spark/entrypoint.sh /opt/ &&\
	cp -r /usr/$SPARK_PACKAGE/examples /opt/spark/examples &&\
	cp -r /usr/$SPARK_PACKAGE/${k8s_tests} /opt/spark/tests &&\
	cp -r /usr/$SPARK_PACKAGE/data /opt/spark/data 

COPY batch/*.jar /opt/spark/jars-examples/
	
WORKDIR /opt/spark/work-dir

WORKDIR $SPARK_HOME

ENTRYPOINT [ "/opt/entrypoint.sh" ]